% Youchao Wang
% University of Cambridge
% February 2021
%
% Model of a typical holographic projection system.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> Camera
% One focal length between each.
%

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 1024;
f = 100e-3;

% Set SLM1
T = imread(['Holo2.bmp']);
X = 2 * pi * double(T) / 255 - pi;
% X = double(T);

SLM1 = zeros(Nx);
SLM1(Nx/2 - size(X,1)/2+1:Nx/2 + size(X,1)/2, ...
     Nx/2 - size(X,2)/2+1:Nx/2 + size(X,2)/2) = X;

%% Calculations

x = linspace(-1000e-5, 1000e-5, Nx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1
% 
F = F.*exp(1i*SLM1); % Diffraction field of SLM1
% 
[F, x] = propFresnel(F, x, lambda, 2*f);
% F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, f); % Incident on camera

%% Plot Reults

% Plot SLM 1
figure; 
imagesc(SLM1);
axis square;
title('SLM1');
xticks(''); yticks('');
colormap gray;

% Plot camera
% F = rot90(F,2);
figure; 
imagesc(x*1e3, x.'*1e3, abs(F));
axis square;
xlabel('mm'); ylabel('mm');
title('Camera');
colormap gray;