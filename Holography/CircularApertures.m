% Youchao Wang
% University of Cambridge
% January 2022
%
% Model of a typical 4f system. This script models Fresnel propagation and
% propagation through a lens separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% One focal length between each.
%
% This is a demonstrator of circular aperture

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

%define radius and center coordinates
r=200; x_c=0; y_c=0;
%generate a coordinate grid
ngrid = 400;
[y,x]=ndgrid(-ngrid:ngrid,-ngrid:ngrid);
%perform calculation
X = (x-x_c).^2+(y-y_c).^2 <= r^2;

% imagesc(X)
% axis square;

lambda = 633e-9;
Nx = 1000;
f = 1;

% Set aperture
APT = zeros(Nx);
APT(Nx/2 - size(X,1)/2+1:Nx/2 + size(X,1)/2, ...
     Nx/2 - size(X,2)/2+1:Nx/2 + size(X,2)/2) = X;

%% Calculations

x = linspace(-1000e-5, 1000e-5, Nx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

F = F.*APT; % Diffraction field of SLM1. This is currently amplitude-based

[F, x] = propFresnel(F, x, lambda, 2*f);
% F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, 2.2*f); % Incident on camera

%% Plot Reults
subplot(1,2,1)
% Plot SLM 1
% figure; 
imagesc(APT);
axis square;
title('SLM1');
xticks(''); yticks('');
colormap gray;

% Plot camera
subplot(1,2,2)
F = rot90(F,2); % Seems to be flipped. Not sure if this is a bug or real.
% figure; 
imagesc(x*1e3, x.'*1e3, abs(F));
axis square;
xlabel('mm'); ylabel('mm');
title('Camera');
colormap gray;