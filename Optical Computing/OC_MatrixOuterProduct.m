% Youchao Wang
% University of Cambridge
% Feburary 2021
%
%
% Model of a typical matrix multiplication system that computes outer product. 
% This script models Fresnel propagation and propagation through a lens separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
% Reference: Goodman, 2nd ed, p288
%

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 150;

xx = 1200;
f = 250e-3;

% A = [0.188488738046487,0.559222149526245,0.821544379744383;0.0876284667298388,0.782325169100250,0.249231966087365;0.472261067606313,0.258999008442634,0.539568513487823];
% B = [0.0567612854641939,0.239659296722370,0.120037834860076;0.837434601900436,0.181770890535275,0.640274520976901;0.412640692890277,0.0219839821602836,0.595653900699343];
Nn = 3;
A = rand(Nn,Nn);
B = rand(Nn,Nn);

% Expected = [0.188488738046487;0.0876284667298388;0.472261067606313] * [0.0567612854641939,0.239659296722370,0.120037834860076];
Expected = A*B;
Expecteddot = dot(A, B);

%% Output

Output = zeros(Nx);

% Calculations
for i = 1:Nn
    
    % Set SLM1

    SLM1 = zeros(Nx);
    for k = 1:Nn
        SLM1((Nx/Nn)*(k-1) + 1: (Nx/Nn)*k, :) = A(k, i);
    end
    
    % Set SLM2

    SLM2 = zeros(Nx);
    for k = 1:Nn
        SLM2(:, (Nx/Nn)*(k-1) + 1: (Nx/Nn)*k) = B(i, k);
    end

    x = linspace(-4000e-5, 4000e-5, xx);
    SMF = CreateSMF(x);
    F = SMF.F; % This is the beam out of the SMF

    [F, x] = propFresnel(F, x, lambda, f);
    F = propLens(F, x,  lambda, f);
    F = F(xx/2 - Nx/2: xx/2 + Nx/2 - 1, ...
        xx/2 - Nx/2: xx/2 + Nx/2 - 1);
 
    F = F.*SLM1; % Diffraction field of SLM1

    x = linspace(-1000e-5, 1000e-5, Nx);

    [F, x] = propFresnel(F, x, lambda, 2*f);
    F = propLens(F, x, lambda, f);
    [F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2

    F = F.* SLM2; % Diffraction field of SLM2, note that the image is flipped,
                    % but for this application we do not need to worry here.
                    % We flip when reading out the result.

    [F, x] = propFresnel(F, x, lambda, 2*f);
    F = propLens(F, x, lambda, f);
    [F,x] = propFresnel(F, x, lambda, 2*f); % Incident on camera
    
    Output = Output + abs(F);
    
    % Plot the evolution on-the-fly
    subplot(1,3,1)
    imagesc(SLM1);
    axis square;
    title('SLM1');
    xticks(''); yticks('');
%     colormap gray;
    colorbar;
    
    subplot(1,3,2)
    imagesc(SLM2);
    axis square;
    title('SLM2');
    xticks(''); yticks('');
%     colormap gray;
    colorbar;
    
    subplot(1,3,3)
    imagesc(fliplr(Output));
    axis square;
    title('Camera');
    xticks(''); yticks('');
%     colormap gray;
    colorbar;
    drawnow
end

% Now compute the average values from output
Real = zeros(Nn, Nn);

for r = 1:Nn
   for c = 1:Nn
       Real(r, c) = mean(Output(size(Output,1) * (r- 1) / Nn + 1 :  size(Output,1) * r/Nn, ...
           size(Output,1) * (c- 1) / Nn + 1 :  size(Output,1) * c/Nn), 'all');
    
   end
end

%% Compare the difference by normalization for both expected and real measurement

normExpected = Expected - min(Expected(:));
normExpected = normExpected ./ max(normExpected(:));

Real = fliplr(Real); % Flipping the matrix

normReal = Real - min(Real(:));
normReal = normReal ./ max(normReal(:));


absError = abs(normReal - normExpected)
relError = (absError ./ abs(normExpected))


%% Plot figures

figure;
subplot(1,2,1)
imagesc(normReal);
axis square;
title('Detected');
xticks(''); yticks('');
% colormap gray;
colorbar;

subplot(1,2,2)
imagesc(normExpected);
axis square;
title('Expected');
xticks(''); yticks('');
% colormap gray;
colorbar;