% Youchao Wang
% University of Cambridge
% March 2021
%
% Model of a typical matrix multiplication system to compute Hadamard
% product.
%
% This script models Fresnel propagation and propagation through a lens 
% separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
%
% Random matrices are used as examples
% TODO: check the row and column ordering

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
f = 1;
rand('twister', 1337);

Nk = 4;%28; % size of kernel
Nkn = 10;%10; % number of kernels Nkn by Nkn
Ng = 10;%10; % size of gap
Nx = Ng + (Nk + Ng) * Nkn;

Nn = Nx * 3;

% Set SLM1
% filling SLM1 with multiple kernels
MM1 = rand(Nk, Nk, Nkn, Nkn) - 0.5;
M1p = zeros(Nk, Nk, Nkn, Nkn);
M1n = zeros(Nk, Nk, Nkn, Nkn);

for r = 1:Nkn
    for c = 1:Nkn
       M1p(:,:,r,c) = MM1(:,:,r,c) + abs(min(MM1(:,:,r,c),[],'all')) + 0.5 * eps(1);% 0.5 * eps(1) for numerical stability
       M1n(:,:,r,c) = ones(Nk, Nk) * abs(min(MM1(:,:,r,c),[],'all')) + 0.5 * eps(1);
    end
end 

M1 = [M1p, M1n; M1p, M1n];

S1 = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       S1(Ng + 1 + (Nk * 2 + Ng)*(r-1): Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2) = M1(:,:, r, c);
    end
end

SLM1 = zeros(Nn);
SLM1(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2) = S1;
 
% Set SLM2
MM2 = rand(Nk, Nk, Nkn, Nkn) - 0.5;
M2p = zeros(Nk, Nk, Nkn, Nkn);
M2n = zeros(Nk, Nk, Nkn, Nkn);

for r = 1:Nkn
    for c = 1:Nkn
       M2p(:,:,r,c) = MM2(:,:,r,c) + abs(min(MM2(:,:,r,c),[],'all')) + 0.5 * eps(1);
       M2n(:,:,r,c) = ones(Nk, Nk) * abs(min(MM2(:,:,r,c),[],'all')) + 0.5 * eps(1);
    end
end 

M2 = [M2p, M2p; M2n, M2n];

S2 = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       S2(Ng + 1 + (Nk * 2 + Ng)*(r-1): Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2) = M2(:,:, r, c);
    end
end

SLM2 = zeros(Nn);
SLM2(Nn/2 - size(S2,1)/2:Nn/2 + size(S2,1)/2-1, ...
     Nn/2 - size(S2,2)/2:Nn/2 + size(S2,2)/2-1) = S2;

%% Calculations
xx = 5 * Nn;
x = linspace(-4000e-5, 4000e-5, xx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

% imagesc(abs(F));
% axis square;
% title('SMF');
% xticks(''); yticks('');
% colormap gray;

F = F(xx/2 - Nn/2: xx/2 + Nn/2 - 1, ...
        xx/2 - Nn/2: xx/2 + Nn/2 - 1);

F = F.*SLM1; % Diffraction field of SLM1

x = linspace(-1000e-5, 1000e-5, Nn);

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2

F = F.*flipud(fliplr(SLM2)); % Image is flipped, so flip matrix by which we're multiplying.
                             % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Incident on camera

M1M2 = abs(F);

% Plot Reults

subplot(1,2,1) 

% Compute expected result
Expect = zeros(Ng + (Nk * 2 + Ng) * Nkn);

for r = 1:Nkn
    for c = 1:Nkn
       Expect(Ng + 1 + (Nk * 2 + Ng)*(r-1): ...
           Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): ...
          Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2) ...
          = M1(:,:, r, c).* M2(:,:, r, c);
    end
end

imagesc(Expect); axis square; title('Expected Result');
colorbar;

subplot(1,2,2) 
imagesc(M1M2); axis square; title('Observed Result');
colorbar;

% Post processing
cropRegion = M1M2(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2);
 
cropResult = zeros(Nk*2, Nk*2, Nkn, Nkn);
for r = 1:Nkn
    for c = 1:Nkn
        cropResult(:,:, r, c) = ...
          cropRegion(Ng + 1 + (Nk * 2 + Ng)*(r-1):...
           Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): ...
           Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2);
    end
end

finalResult = zeros(Nk, Nk, Nkn, Nkn);
for r = 1:Nk
    for c = 1:Nk
        finalResult(r, c, :, :) = cropResult(r, c, :, :)...
                    - cropResult(r, c + Nk, :, :)...
                    - cropResult(r+Nk, c, :, :) ...
                    + cropResult(r+Nk, c+Nk, :, :);
    end    
end


%% Normalizing the observed result
output = zeros(Nk, Nk, Nkn, Nkn);
M1M2Exp = MM1 .* MM2;
M1M2Abs = finalResult;
for rk = 1:Nkn
    for ck = 1:Nkn
        M1M2ExpRC = M1M2Exp(:,:,rk,ck);
        [M, I] = max(M1M2ExpRC(:));
        [Ir, Ic] = ind2sub(size(M1M2ExpRC),I);
        A = M1M2Abs(Ir,Ic,rk,ck) / M1M2ExpRC(Ir,Ic);
        output(:,:,rk,ck) = M1M2Abs(:,:,rk,ck) ./ A; 
    end
end


%%%% debug use
% subplot(1,2,1) 
% 
% imagesc(MM1(:,:,1,2) .* MM2(:,:,1,2)); axis square; title('Expected Result');
% colorbar;
% 
% subplot(1,2,2) 
% imagesc(finalResult(:,:,1,2)); axis square; title('Observed Result');
% colorbar;
