% Youchao Wang
% University of Cambridge
% Feburary 2021
%
%
% This is a demonstration of a neural network using FSO simulations
% Reference: Grokking Deep Learning
%

clc; clear variables; close all;
addpath('..\Function Library');
addpath('..\Data');

rand('twister', 1337);% maek sure we use the same random seed as python numpy

filenameImagesTrain = 'train-images.idx3-ubyte';
filenameLabelsTrain = 'train-labels.idx1-ubyte';
filenameImagesTest = 't10k-images.idx3-ubyte';
filenameLabelsTest = 't10k-labels.idx1-ubyte';

XTrain = processMNISTImages(filenameImagesTrain);% size: 28,28,1,10000
YTrain = processMNISTLabels(filenameLabelsTrain);
XTest = processMNISTImages(filenameImagesTest); % size: 28,28,1,60000
YTest = processMNISTLabels(filenameLabelsTest); 

number = 1000;
%% Take 1000
% Images = XTrain(1:28,1:28,1,1:number); 
Images = reshape(XTrain(1:28,1:28,1,1:number), [28*28, number]);
Images = permute(Images, [2 1]);
% % Images = reshape(Images, [28, 28, 15]);
% % imagesc(extractdata(Images(:,:,1)))
% % imagesc(extractdata(Images(:,:, 1,1)))
Lbls = YTrain(1:number);

OneHotLabels = zeros(size(Lbls,1),10);

for i = 1:size(Lbls)
   OneHotLabels(i, Lbls(i)) = 1; % labels are categorical
                                   % e.g. labels(3) = 4, 
                                   % double(labels(3)) = 5
end

Labels = OneHotLabels;

numberTest = 1000;
% TestImages = XTest;
TestImages = reshape(XTest(1:28,1:28,1,1:numberTest), [28*28, numberTest]);
% TestImages = reshape(XTest(1:28,1:28,1,:), [28*28, size(XTest, 4)]);
TestImages = permute(TestImages, [2 1]);
TestLabels = zeros(size(YTest,1), 10);

for i = 1:size(YTest)
   TestLabels(i, YTest(i)) = 1;
end

[Alpha, Iterations, HiddenSize, PixelsPerImage, NumLabels] = deal(0.005, 300, 100 ,784, 10);

weights01 = 0.2 * rand(PixelsPerImage, HiddenSize) - 0.1;
weights12 = 0.2 * rand(HiddenSize, NumLabels) - 0.1;


for j = 1:Iterations
   [Error, CorrectCnt] = deal(0.0, 0);
   
   for i = 1:size(Images, 1)
      layer0 = extractdata(Images(i, :));
      
      layer1 = relu(layer0 * weights01);
      % Drop out mask
      DropoutMask = randi([0,1], size(layer1));
      layer1 = layer1 .* DropoutMask .* 2; % multiply by two to counter the effect of dropout
      
      layer2 = layer1 * weights12;
      Error  = Error + sum((Labels(i, :) - layer2).^2);
      [M1, ILayer2] = max(layer2(:));
      [ML, ILabels] = max(Labels(i, :));
      CorrectCnt = CorrectCnt + (ILayer2 == ILabels);
      
      layer2Delta = Labels(i,:) - layer2;
      layer1Delta = layer2Delta * weights12' .* reluDeriv(layer1);
      layer1Delta = layer1Delta .* DropoutMask;
      
      weights12 = weights12 + Alpha .* (layer1' * layer2Delta);
      weights01 = weights01 + Alpha .* (layer0' * layer1Delta);
      
   end
% % %  imagesc(reshape(layer0, [28, 28]))
    if mod(j, 100) == 0
       testError = 0.0;
       testCorrectCnt = 0;
       
       for i = 1:size(TestImages,1)
           layer0 = extractdata(TestImages(i, :));
           layer1 = relu(layer0 * weights01);
           layer2 = layer1 * weights12;
           
           testError  = testError + sum((TestLabels(i,: ) - layer2).^2);
           [M2, ILayer22] = max(layer2(:));
           [ML2, ILabels2] = max(TestLabels(i, :));
           testCorrectCnt = testCorrectCnt + (ILayer22 == ILabels2);
       end
      

    end
   
   
end

   Error/size(Images,2)
   CorrectCnt/size(Images,2)
         testError / size(TestImages, 1)
      testCorrectCnt / size(TestImages, 1)

%% define functions
function output = relu(x)
    output = (x>=0) .* x;
end

function output = reluDeriv(x)
    output = (x>=0);
end

