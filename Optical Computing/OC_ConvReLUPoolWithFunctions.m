% Youchao Wang
% University of Cambridge
% Feburary 2021
%
%
% Model of a typical matrix multiplication system that performs convolution. 
% This script models Fresnel propagation and propagation through a lens separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
%

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 1000;
f = 1;

M2 = [1, 0, -1; 0, 0, 0; -1, 0, 1]; % edge detection kernel

psize = 2;
pstride = 2;

T = imread('Mandrill.png');
X = double(rgb2gray(T));

x = linspace(-1000e-5, 1000e-5, Nx);

%% Set SLM1 to display an image
SLM1 = LoadSLMDisplay(Nx, X, M2, 1);
 
%% Set SLM2 to display FT'ed kernel
SLM2 = LoadSLMDisplay(Nx, X, M2, 2);

%% Calculations

SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF
[F, x] = propConv(F, x, lambda, f, SLM1, SLM2);

%% Apply ReLU

% Crop the detected image, feature_map
FMap = F(Nx/2 - size(X,1)/2+1:Nx/2 + size(X,1)/2, ...
     Nx/2 - size(X,2)/2+1:Nx/2 + size(X,2)/2);

FReLU = compReLU(FMap);

%% Apply max pooling

FPool = compMaxPool(FReLU, Nx, X, psize, pstride);

%% Plot Reults
subplot(1,5,1)
% Plot SLM 1
% figure;
imagesc(SLM1);
axis square;
title('SLM1');
xticks(''); yticks('');
colormap gray;

% Plot SLM2
% figure;
subplot(1,5,2)
imagesc(SLM2);
axis square;
title('SLM2');
xticks(''); yticks('');
colormap gray;

% Plot camera
subplot(1,5,3)
F = rot90(real(F),2);
% figure;
imagesc(x*1e3, x.'*1e3, F); % take the real part to save negativity, although
                                   % this is not the case in real senario
% imagesc(abs(F));
axis square;
xlabel('mm'); ylabel('mm');
title('Camera');
colormap gray;

% Plot ReLU
subplot(1,5,4)
imagesc(rot90(FReLU,2));
axis square;
title('ReLU');
xticks(''); yticks('');
colormap gray;

% Plot Pooling
subplot(1,5,5)
imagesc(rot90(FPool,2));
axis square;
title('Pooling');
xticks(''); yticks('');
colormap gray
%% 
