% Youchao Wang
% University of Cambridge
% Feburary 2021
%
%
% This is a demonstration of a neural network using FSO simulations
% Reference: Grokking Deep Learning
%

warning('off','all');
clc; clear variables; close all;
addpath('..\Function Library');
addpath('..\Data');

rand('twister', 1337);% maek sure we use the same random seed as python numpy

filenameImagesTrain = 'train-images.idx3-ubyte';
filenameLabelsTrain = 'train-labels.idx1-ubyte';
filenameImagesTest = 't10k-images.idx3-ubyte';
filenameLabelsTest = 't10k-labels.idx1-ubyte';

XTrain = processMNISTImages(filenameImagesTrain);% size: 28,28,1,10000
YTrain = processMNISTLabels(filenameLabelsTrain);
XTest = processMNISTImages(filenameImagesTest); % size: 28,28,1,60000
YTest = processMNISTLabels(filenameLabelsTest); 

number = 1000;
Images = reshape(XTrain(1:28,1:28,1,1:number), [28*28, number]);
Images = permute(Images, [2 1]);

subplot(1,2,1)
curve1 = animatedline('LineWidth', 2);
% hold on
% plot(t,Y1);
subplot(1,2,2) 
curve2 = animatedline('LineWidth', 2);
% hold on
% plot(t,Y2);

Lbls = YTrain(1:number);

OneHotLabels = zeros(size(Lbls,1),10);

for i = 1:size(Lbls)
   OneHotLabels(i, Lbls(i)) = 1; % labels are categorical
                                   % e.g. labels(3) = 4, 
                                   % double(labels(3)) = 5
end

Labels = OneHotLabels;

numberTest = 1000;
TestImages = reshape(XTest(1:28,1:28,1,1:numberTest), [28*28, numberTest]);
TestImages = permute(TestImages, [2 1]);
TestLabels = zeros(size(YTest,1), 10);

for i = 1:size(YTest)
   TestLabels(i, YTest(i)) = 1;
end

[Alpha, Iterations, HiddenSize, PixelsPerImage, NumLabels] = deal(0.005, 200, 100 ,784, 10);

weights01 = 0.2 * rand(PixelsPerImage, HiddenSize) - 0.1;
weights12 = 0.2 * rand(HiddenSize, NumLabels) - 0.1;

% Take the values and use it for simulation
weights01Re = reshape(weights01, [28 28 HiddenSize]);
weights12Re = reshape(weights12, [10 10 10]);

for j = 1:Iterations
   [Error, CorrectCnt] = deal(0.0, 0);
   [ErrorRe, CorrectCntRe] = deal(0.0, 0);
   
   for i = 1:size(Images, 1)
      layer0 = extractdata(Images(i, :));
      layer1 = relu(layer0 * weights01);
      
      layer2 = layer1 * weights12;
      Error  = Error + sum((Labels(i, :) - layer2).^2);
      [~, ILayer2] = max(layer2(:));
      [~, ILabels] = max(Labels(i, :));
      CorrectCnt = CorrectCnt + (ILayer2 == ILabels);
      
      layer2Delta = Labels(i,:) - layer2;
      layer1Delta = layer2Delta * weights12' .* reluDeriv(layer1);
      
      weights12 = weights12 + Alpha .* (layer1' * layer2Delta);
      weights01 = weights01 + Alpha .* (layer0' * layer1Delta);
      
      %%%%%%%%%%%%%%%
      %%% Alternatively, we use element-wise multiplication with sum
      layer0Re = reshape(layer0, [28 28]);
      
      for z = 1:HiddenSize
          temp = weights01Re(:,:,z);
          
          % Expected
          layer1Re(1,z) = relu(sum(layer0Re .* temp, 'All'));
          
          % Simulation
          % Make sure layer0Re doesn't contain 0
%           layer0Re(layer0Re == 0) = 1e-100;
%           hadamard = compHadamardProductWithNeg(layer0Re, temp);
%           layer1Re(1,z) = relu(sum(hadamard, 'All'));
      end
      
      layer1Re = reshape(layer1Re, [10 10]);
 
      for z = 1:10
          temp2 = weights12Re(:,:,z);
          
          % Expected
          layer2Re(1, z) = sum(layer1Re .* temp2, 'All');
          
          % Simulation
%           hadamard2 = compHadamardProductWithNeg(layer1Re, temp2);
%           layer2Re(1, z) = sum(hadamard2, 'All');
      end
      
      ErrorRe  = ErrorRe + sum((Labels(i, :) - layer2Re).^2);
      [~, ILayer2] = max(layer2Re(:));
      [~, ILabels] = max(Labels(i, :));
      CorrectCntRe = CorrectCntRe + (ILayer2 == ILabels);
      
      layer1Re = reshape(layer1Re, [1 100]);
      layer0Re = reshape(layer0Re, [1 784]);
    
      weights01Re = reshape(weights01Re, [784 HiddenSize]);
      weights12Re = reshape(weights12Re, [100 10]);
      
      layer2DeltaRe = Labels(i,:) - layer2Re;
      layer1DeltaRe = layer2DeltaRe * weights12Re' .* reluDeriv(layer1Re);
      
      weights12Re = weights12Re + Alpha .* (layer1Re' * layer2DeltaRe);
      weights01Re = weights01Re + Alpha .* (layer0Re' * layer1DeltaRe);
      
      weights01Re = reshape(weights01Re, [28 28 HiddenSize]);
      weights12Re = reshape(weights12Re, [10 10 10]);
      
      %%% abs(layer2-layer2Re) < 1e4*eps(min(abs(layer2),abs(layer2Re)))
      %%%%%%%%%%%%%%%
      
   end
% % %  imagesc(reshape(layer0, [28, 28]))
   Y1 = CorrectCnt/size(Images,1);
   Y2 = CorrectCntRe / size(Images,1);  
    
   addpoints(curve1,j,Y1);
   addpoints(curve2,j,Y2);
   drawnow


    if mod(j, 100) == 0
       testError = 0.0;
       testCorrectCnt = 0;
       
       testErrorRe = 0.0;
       testCorrectCntRe = 0;
       
       for i = 1:size(TestImages,1)
           
           % We are not simulating the process here, instead just taking
           % the derived values of weights01 and weights12
           layer0 = extractdata(TestImages(i, :));
           layer1 = relu(layer0 * weights01);
           layer2 = layer1 * weights12;
           
           testError  = testError + sum((TestLabels(i,: ) - layer2).^2);
           [M2, ILayer22] = max(layer2(:));
           [ML2, ILabels2] = max(TestLabels(i, :));
           testCorrectCnt = testCorrectCnt + (ILayer22 == ILabels2);
           
           %%%%%%%%%%%%%%%%%%%%%%
           % Expected
           weights01Re = reshape(weights01Re, [784 100]);
           weights12Re = reshape(weights12Re, [100 10]);
           layer0Re = extractdata(TestImages(i, :));
           layer1Re = relu(layer0Re * weights01Re);
           layer2Re = layer1Re * weights12Re;
           
           testErrorRe  = testErrorRe + sum((TestLabels(i,: ) - layer2Re).^2);
           [M2, ILayer22Re] = max(layer2Re(:));
           [ML2, ILabels2Re] = max(TestLabels(i, :));
           testCorrectCntRe = testCorrectCntRe + (ILayer22Re == ILabels2Re);
           
           weights01Re = reshape(weights01Re, [28 28 100]);
           weights12Re = reshape(weights12Re, [10 10 10]);
           %%%%%%%%%%%%%%%%%%%%%%
           
           % Construct confusion matrix
           g1(i) = ILayer22; % predict
           g2(i) = ILabels2; % expect
           g1Re(i) = ILayer22Re; % predict
           g2Re(i) = ILabels2Re; % expect
       end
      

    end
   
   
end

%    Error/size(Images,1)
%    CorrectCnt/size(Images,1)
%    testError / size(TestImages, 1)
%    testCorrectCnt / size(TestImages, 1)

%% define functions
function output = relu(x)
    output = (x>=0) .* x;
end

function output = reluDeriv(x)
    output = (x>=0);
end

