% Ralf Mouthaan
% University of Cambridge
% January 2021
%
% Modifed Youchao Wang, 2021
%
% Model of a typical matrix multiplication system to compute Hadamard
% product.
% This script models Fresnel propagation and propagation through a lens 
% separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
%
% Random matrices are used as examples

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 1050; % 70 + (28 + 70)*10
f = 1;

Nk = 28; % size of kernel
Nkn = 10; % number of kernels Nkn by Nkn
Ng = 10; % size of gap
Nx = Ng + (Nk + Ng) * Nkn;

Nn = Nx * 3;

% Set SLM1
% filling SLM1 with multiple kernels
M1 = 2 * pi * rand(Nk, Nk, Nkn, Nkn);

S1 = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       S1(Ng + 1 + (Nk + Ng)*(c-1): Ng + (Nk + Ng)*(c-1) + Nk, ...
          Ng + 1 + (Nk + Ng)*(r-1): Ng + (Nk + Ng)*(r-1) + Nk) = M1(:,:, r, c);
    end
end

SLM1 = zeros(Nn);
SLM1(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2) = S1;
 
% Set SLM2
M2 = 2 * pi * rand(Nk, Nk, Nkn, Nkn);

S2 = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       S2(Ng + 1 + (Nk + Ng)*(c-1): Ng + (Nk + Ng)*(c-1) + Nk, ...
          Ng + 1 + (Nk + Ng)*(r-1): Ng + (Nk + Ng)*(r-1) + Nk) = M2(:,:, r, c);
    end
end

SLM2 = zeros(Nn);
SLM2(Nn/2 - size(S2,1)/2:Nn/2 + size(S2,1)/2-1, ...
     Nn/2 - size(S2,2)/2:Nn/2 + size(S2,2)/2-1) = S2;

%% Calculations
xx = 5 * Nn;
x = linspace(-4000e-5, 4000e-5, xx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

% imagesc(abs(F));
% axis square;
% title('SMF');
% xticks(''); yticks('');
% colormap gray;

F = F(xx/2 - Nn/2: xx/2 + Nn/2 - 1, ...
        xx/2 - Nn/2: xx/2 + Nn/2 - 1);

F = F.*SLM1; % Diffraction field of SLM1

x = linspace(-1000e-5, 1000e-5, Nn);

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2

F = F.*flipud(fliplr(SLM2)); % Image is flipped, so flip matrix by which we're multiplying.
                             % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Incident on camera


M1M2 = F;

% Plot Reults

subplot(1,2,1) 

% Compute expected result
Expect = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       Expect(Ng + 1 + (Nk + Ng)*(c-1): Ng + (Nk + Ng)*(c-1) + Nk, ...
          Ng + 1 + (Nk + Ng)*(r-1): Ng + (Nk + Ng)*(r-1) + Nk) = M1(:,:, r, c).* M2(:,:, r, c);
    end
end
imagesc(Expect); axis square; title('Expected Result');
colorbar;

% M1M2 = F(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
%      Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2);
 
subplot(1,2,2) 
imagesc(abs(M1M2)); axis square; title('Observed Result');
colorbar;
