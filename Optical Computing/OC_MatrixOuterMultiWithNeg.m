% Youchao Wang
% University of Cambridge
% January 2022
%
%
% Model of a typical matrix multiplication system that computes outer
% product, with one-step multi-kernel.
% This script models Fresnel propagation and propagation through a lens separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
% Reference: Goodman, 2nd ed, p288, p289
%
% TODO: Add pixel pitch

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;

f = 1;

Nk = 4; % size of kernel
Ng = 6; % size of gap

Nkr = 2; % number of rows
Nkc = 4; % number of columns
Nx = Ng + (Nk + Ng) * Nkc;

Nn = 200; % pixels of SLM

rand('twister', 1337);

AA = rand(Nk,Nk) - 0.5;
BB = rand(Nk,Nk) - 0.5;
Expected = AA*BB;

%% Pre-processing

Ap = AA + abs(min(AA(:))) + 0.5 * eps(1); % 0.5 * eps(1) for numerical stability
An = ones(Nk, Nk) * abs(min(AA(:))) + 0.5 * eps(1);

Bp = BB + abs(min(BB(:))) + 0.5 * eps(1);
Bn = ones(Nk, Nk) * abs(min(BB(:))) + 0.5 * eps(1);

A = [Ap, An; An, Ap];
B = [Bp, Bn; Bn, Bp];

%% Load SLMs

% Previously we use an iterative process,
% in fact this could be simplified with the idea of
% multi-kernel.
% Set SLM1
% filling SLM1 with multiple kernels
S1 = zeros(Nx);
M1 = zeros(Nk*2,Nk*2, Nkc);

% Determining the kernels for multiplication
cnt = 1;
for i = 1:(Nk*2) 
    for k = 1:(Nk*2)
        M1(k, :, cnt) = A(k, i);
    end
    cnt = cnt + 1;
end

% Arranging the 'canvas'
cnt = 1;
for r = 1:Nkr
    for c = 1:Nkc
        S1(Ng + 1 + (Nk + Ng)*(r-1): Ng + (Nk + Ng)*(r-1) + Nk*2, ...
            Ng + 1 + (Nk + Ng)*(c-1): Ng + (Nk + Ng)*(c-1) + Nk*2) = M1(:,:, cnt);
        cnt = cnt + 1;
    end
end

SLM1 = zeros(Nn);
SLM1(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2) = S1;
 
% Set SLM2
M2 = zeros(Nk*2,Nk*2, Nkc);
S2 = zeros(Nx);

cnt = 1;
for i = 1:(Nk*2) 
    for k = 1:(Nk*2)
        M2(:, k, cnt) = B(i, k);
    end
    cnt = cnt + 1;
end

cnt = 1;
for r = 1:Nkr
    for c = 1:Nkc
        S2(Ng + 1 + (Nk + Ng)*(r-1): Ng + (Nk + Ng)*(r-1) + Nk*2, ...
            Ng + 1 + (Nk + Ng)*(c-1): Ng + (Nk + Ng)*(c-1) + Nk*2) = M2(:,:, cnt);
        cnt = cnt + 1;
    end
end

SLM2 = zeros(Nn);
SLM2(Nn/2 - size(S2,1)/2:Nn/2 + size(S2,1)/2-1, ...
     Nn/2 - size(S2,2)/2:Nn/2 + size(S2,2)/2-1) = S2;

%% Calculations

xx = 5 * Nn;
x = linspace(-4000e-5, 4000e-5, xx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f); % Now we have a 'collimated' beam
% [F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1
% mesh(angle(F));
% mesh(abs(F))
% imagesc(abs(F));
% axis square;
% title('SMF');
% xticks(''); yticks('');
% colormap gray;
% colorbar;


F = F(xx/2 - Nn/2: xx/2 + Nn/2 - 1, ...
        xx/2 - Nn/2: xx/2 + Nn/2 - 1);

mesh(abs(F))
% imagesc(abs(F));
% axis square;
% title('SMF');
% xticks(''); yticks('');
% colormap gray;
% colorbar;

F = F.*SLM1; % Diffraction field of SLM1

x = linspace(-1000e-5, 1000e-5, Nn);

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2
ababa = rot90(abs(F),2);
F = F.* rot90(SLM2,2); % Image is flipped, so flip matrix by which we're multiplying.
                             % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Incident on camera

Output = abs(F);
mesh(Output)
% Output = flipud(fliplr(Output));

% Reconstruct for addition
ResRaw = zeros(Nk*2,Nk*2, Nkc);
Comp = zeros(Nk*2,Nk*2, Nkc);

Res = Output(Nn/2 - size(S2,1)/2:Nn/2 + size(S2,1)/2-1, ...
     Nn/2 - size(S2,2)/2:Nn/2 + size(S2,2)/2-1);

cnt = 1;
for r = 1:Nkr
    for c = 1:Nkc
        ResRaw(:,:, cnt) = Res(1+ Ng + 1 + (Nk + Ng)*(r-1): 1+ Ng + (Nk + Ng)*(r-1) + Nk*2, ...
           1 + Ng + 1 + (Nk + Ng)*(c-1): 1 + Ng + (Nk + Ng)*(c-1) + Nk*2);

        subplot(Nkr, Nkc, cnt)
        imagesc(ResRaw(:,:,cnt));
        axis square;
        title("Detected"+ cnt);
        xticks(''); yticks('');
        drawnow

        cnt = cnt + 1;
    end
end

cnt = 1;
TSLM2 = zeros(Nn);
TSLM2(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2) = S2;
z = SLM1 .* TSLM2;
ZRes = z(Nn/2 - size(S2,1)/2:Nn/2 + size(S2,1)/2-1, ...
     Nn/2 - size(S2,2)/2:Nn/2 + size(S2,2)/2-1);

figure;
for r = 1:Nkr
    for c = 1:Nkc
        Comp(:,:, cnt) = ZRes(1+ Ng + 1 + (Nk + Ng)*(r-1): 1+ Ng + (Nk + Ng)*(r-1) + Nk*2, ...
           1 + Ng + 1 + (Nk + Ng)*(c-1): 1 + Ng + (Nk + Ng)*(c-1) + Nk*2);

        subplot(Nkr, Nkc, cnt)
        imagesc(Comp(:,:,cnt));
        axis square;
        title("Expected" + cnt);
        xticks(''); yticks('');
        drawnow
        
        cnt = cnt + 1;
    end
end

Result = zeros(Nk*2, Nk*2);
for i = 1:Nk*2
    Result = Result + ResRaw(:,:, i);
    
end

resres = zeros(Nk*2, Nk*2);
for i = 1:Nk*2
    resres = resres + Comp(:,:, i);
   
end

    figure;
    subplot(1,2,1)
    imagesc(Result);
    axis square;
    title("Detected");
    xticks(''); yticks('');
    colorbar;
    
    subplot(1,2,2)
    imagesc(resres);
    axis square;
    title("Expected");
    xticks(''); yticks('');
    colorbar;

% Then do the substraction electronically

Real = zeros(Nk, Nk);

for r = 1:Nk
   for c = 1:Nk
       Real(r, c) = Result(r + Nk, c + Nk) - Result(r, c + Nk);
   end
end

ReRe = zeros(Nk, Nk);

for r = 1:Nk
   for c = 1:Nk
       ReRe(r, c) = - resres(r, c + Nk) + resres(r + Nk, c + Nk);
   end
end 

%% Compare the difference by normalization for both expected and real measurement

normExpected = Expected - min(Expected(:));
normExpected = normExpected ./ max(normExpected(:));

% Real = ReRe;

normReal = Real - min(Real(:));
normReal = normReal ./ max(normReal(:));

absError = abs(normReal - normExpected)
relError = (absError ./ abs(normExpected))


%% Plot figures

figure;
subplot(1,2,1)
imagesc(Real);
axis square;
axis equal;
axis image;
title('Detected');
xticks(''); yticks('');
% colormap gray;
colorbar;

subplot(1,2,2)
imagesc(Expected);
axis square;
title('Expected');
xticks(''); yticks('');
% colormap gray;
colorbar;