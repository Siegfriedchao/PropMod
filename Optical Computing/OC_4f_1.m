% Ralf Mouthaan, Youchao Wang
% University of Cambridge
% 2021 - 2022
%
% Model of a typical 4f system. This script models propagation from one
% focal point of a lens to the other as a Fourier transform.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% One focal length between each.
%
% This is a correlator, so as an example, a low-pass filter of the mandrill
% is implemented.

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 1000;
f = 500e-3;

% Set SLM1
load('mandrill', 'X');
SLM1 = zeros(Nx);
SLM1(Nx/2 - size(X,1)/2+1:Nx/2 + size(X,1)/2, ...
     Nx/2 - size(X,2)/2+1:Nx/2 + size(X,2)/2) = X;

% Set SLM2
SLM2 = zeros(Nx);
% SLM2(Nx/2-20:Nx/2+20,Nx/2-20:Nx/2+20) = 1;
SLM2(Nx/2-round(Nx/25):Nx/2+round(Nx/25),Nx/2-round(Nx/25):Nx/2+round(Nx/25)) = 1;

%% Calculations

x = linspace(-200e-5, 200e-5, Nx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF
[F, x] = propFFT(F, x, lambda, f); % This is the illumination of SLM1
F = F.*SLM1; % Diffraction field of SLM1
F = propFFT(F, x, lambda, f); % Illumination of SLM2
F = F.*SLM2; % Diffraction field of SLM2
F = propFFT(F, x, lambda, f); % Incident on camera

%% Plot Reults
subplot(1,3,1)
% Plot SLM 1
% figure; 
imagesc(SLM1);
axis square;
title('SLM1');
xticks(''); yticks('');
colormap gray;

% Plot SLM2
% figure;
subplot(1,3,2)
imagesc(SLM2);
axis square;
title('SLM2');
xticks(''); yticks('');
colormap gray

% Plot camera
F = flipud(fliplr(F)); % Seems to be flipped. Not sure if this is a bug or real.
subplot(1,3,3)
% figure; 
imagesc(x*1e3, x.'*1e3, abs(F));
axis square;
xticks(''); yticks('');
% xlabel('mm'); ylabel('mm');
title('Camera');
colormap gray;

figure;
B = rescale(abs(F));
mesh(B)