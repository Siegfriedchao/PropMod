% Youchao Wang
% University of Cambridge
% March 2021
%
% Model of a typical matrix multiplication system to compute Hadamard
% product.
% This script models Fresnel propagation and propagation through a lens 
% separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
%
% Random matrices are used as examples

clc; clear variables; close all;
addpath('../Function Library');
addpath('..\Debug');
% Pre-processing
% load('layer0Re.mat');
% load('temp.mat');
% 
% MM1 = layer0Re;
% MM2 = temp;
% 
% Nn = size(MM1, 1);
%% User-entered parameters

lambda = 633e-9;
Nx = 1000;
f = 1;
rand('twister', 1337);

% Pre-processing
Nn = 2;

MM1 = rand(Nn) - 0.5;
MM2 = rand(Nn) - 0.5;

M1p = MM1 + abs(min(MM1(:))) + 0.00001;
M1n = ones(Nn, Nn) * abs(min(MM1(:))) + 0.00001;

M2p = MM2 + abs(min(MM2(:))) + 0.00001;
M2n = ones(Nn, Nn) * abs(min(MM2(:))) + 0.00001;

M1 = [M1p, M1n; M1p, M1n];
M2 = [M2p, M2p; M2n, M2n];

% Set SLM1
SLM1 = zeros(Nx);
SLM1(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
     Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2) = M1;

% Set SLM2
SLM2 = zeros(Nx);
SLM2(Nx/2 - size(M2,1)/2:Nx/2 + size(M2,1)/2-1, ...
     Nx/2 - size(M2,2)/2:Nx/2 + size(M2,2)/2-1) = M2; % We have some kind of off-by-one error that I don't understand but am correcting here.

%% Calculations

x = linspace(-1000e-5, 1000e-5, Nx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

F = F.*SLM1; % Diffraction field of SLM1

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2

F = F.*flipud(fliplr(SLM2)); % Image is flipped, so flip matrix by which we're multiplying.
                             % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Incident on camera


M1M2 = F(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
     Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2);

M1M2 = abs(M1M2);

result = zeros(Nn, Nn);
for r = 1:Nn
    for c = 1:Nn
        result(r, c) = M1M2(r, c) - M1M2(r, c + Nn) - M1M2(r+Nn, c) + M1M2(r+Nn, c+Nn);
    end
end

%% Normalizing the observed result

M1M2Exp = MM1 .* MM2;
M1M2Abs = result;
[M, I] = max(M1M2Exp(:));
[Ir, Ic] = ind2sub(size(M1M2Exp),I)
A = M1M2Abs(Ir,Ic) / M1M2Exp(Ir,Ic);
output = M1M2Abs ./ A;

% Plot Reults

subplot(1,2,1) 

imagesc(MM1.*MM2); axis square; title('Expected Result');
colorbar;

subplot(1,2,2) 
imagesc(output); axis square; title('Observed Result');
colorbar;
