clc; clear; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 1000;
f = 500e-3;

% Set SLM1

pixel_pitch = 8e-6;
pixel_size = 7.8e-6;
simulation_resolution = 1e-7;
SLM_x = 80;
SLM_y = SLM_x;

pixel_gap = pixel_pitch - pixel_size;
source_x = SLM_x * pixel_size + (SLM_x - 1) * pixel_gap;
source_x_res = (source_x / simulation_resolution);
source_y = SLM_y * pixel_size + (SLM_y - 1) * pixel_gap;
source_y_res = uint32(source_y / simulation_resolution);

% load('mandrill', 'X');
X = imread('/Users/youchaowang/Documents/GitLab/python_scripts/CGH/input/checkerboard_512.bmp');


T1 = uint32(pixel_size/simulation_resolution);
T2 = uint32(pixel_gap/simulation_resolution);
X = imresize(X, 1/16, 'nearest');
IM = imresize(X, T1, 'nearest');

IM_with_gap = zeros(size(IM,1)+(size(X,1)-1)*T2, size(IM,2)+(size(X,2)-1)*T2);

place_x = [];
place_y = [];
for x_x = 1:size(X,1)
    place_x(end+1:end+T1) = uint32((x_x-1)*T1 + (x_x-1)*T2 + 1) ...
         : uint32(x_x*T1 + (x_x-1)*T2);
end

for y_y = 1:size(X,2)
    place_y(end+1:end+T1) = uint32((y_y-1)*T1 + (y_y-1)*T2 + 1) ...
         : uint32(y_y*T1 + (y_y-1)*T2);
end

for x_x = 1:length(place_x)
    for y_y = 1:length(place_y)
        IM_with_gap(place_x(x_x), place_y(y_y)) = IM(x_x, y_y);
    end
end

%%

SLM1 = zeros(uint32(source_x_res), source_y_res);
SLM1(source_x_res/2 - size(IM_with_gap,1)/2+1:source_x_res/2 + size(IM_with_gap,1)/2, ...
     source_y_res/2 - size(IM_with_gap,2)/2+1:source_y_res/2 + size(IM_with_gap,2)/2) = IM_with_gap;

% Set Fourier plane to pass everything
SLM2 = ones(uint32(source_x_res), source_y_res);
% SLM2 = zeros(uint32(source_x_res), source_y_res);
% Nx = uint32(source_x_res);
% SLM2(Nx/2-round(Nx/10):Nx/2+round(Nx/10),Nx/2-round(Nx/10):Nx/2+round(Nx/10)) = 1;

% x = linspace(-200e-6,200e-6, source_x_res);
x = linspace(-source_x_res*simulation_resolution,source_x_res*simulation_resolution, source_x_res);
SMF.x = x;
SMF.w0 = 8.2e-2;

% Gaussian beam from SMF
x_mesh = x;
y_mesh = x';
SMF.F = exp(2*(-x_mesh.^2 - y_mesh.^2)/SMF.w0^2);
SMF.F = SMF.F/max(max(SMF.F));  
F = SMF.F; % This is the beam out of the SMF

%% Fresnel 
% [F, x] = propFresnel(F, x, lambda, f);
% F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

F = F.*SLM1; % Diffraction field of SLM1. This is currently amplitude-based

% x = linspace(-100e-5, 100e-5, source_x_res);

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Illumination of SLM2

F = F.*SLM2; % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Incident on camera

%% Fraunhofer
% [F, x] = propFFT(F, x, lambda, f); % This is the illumination of SLM1
% F = F.*SLM1; % Diffraction field of SLM1
% F = propFFT(F, x, lambda, f); % Illumination of SLM2
% F = F.*SLM2; % Diffraction field of SLM2
% F = propFFT(F, x, lambda, f); % Incident on camera

% %% Plot Reults
% subplot(1,3,1)
% % Plot SLM 1
% % figure; 
% imagesc(SLM1);
% axis square;
% title('SLM1');
% xticks(''); yticks('');
% colormap gray;

% % Plot SLM2
% % figure;
% subplot(1,3,2)
% imagesc(SLM2);
% axis square;
% title('SLM2');
% xticks(''); yticks('');
% colormap gray
% 
% Plot camera
F = flipud(fliplr(F)); % Seems to be flipped. Not sure if this is a bug or real.
% subplot(1,3,3)
% figure; 
imagesc(x*1e3, x.'*1e3, abs(F));
axis square;
xticks(''); yticks('');
% xlabel('mm'); ylabel('mm');
title('Camera');
colormap gray;