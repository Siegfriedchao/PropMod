% Ralf Mouthaan
% University of Cambridge
% January 2021
%
% Modifed Youchao Wang, 2021
%
% Model of a typical matrix multiplication system to compute Hadamard
% product.
% This script models Fresnel propagation and propagation through a lens 
% separately.
%
% The system being modelled is:
% SMF --> Lens 1 --> SLM 1 --> Lens 2 --> SLM 2 --> Lens 3 --> Camera
% Two focal lengths between each
%
% Random matrices are used as examples

clc; clear variables; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;
Nx = 1000;
f = 1;

% Set SLM1
M1 = 2 * pi * rand(100);
SLM1 = zeros(Nx);
SLM1(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
     Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2) = M1;

% Set SLM2
SLM2 = zeros(Nx);
M2 = 2 * pi * rand(100);
SLM2(Nx/2 - size(M2,1)/2:Nx/2 + size(M2,1)/2-1, ...
     Nx/2 - size(M2,2)/2:Nx/2 + size(M2,2)/2-1) = M2; % We have some kind of off-by-one error that I don't understand but am correcting here.

%% Calculations

x = linspace(-1000e-5, 1000e-5, Nx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
% [F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

imagesc(abs(F));
axis square;
title('SMF');
xticks(''); yticks('');
colormap gray;

F = F.*SLM1; % Diffraction field of SLM1

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2

F = F.*flipud(fliplr(SLM2)); % Image is flipped, so flip matrix by which we're multiplying.
                             % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Incident on camera


M1M2 = F(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
     Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2);

% Plot Reults

% M1M2 = F(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
%      Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2);
 
% subplot(1,2,2) 
% imagesc(abs(M1M2)); axis square; title('Observed Result');
% colorbar;

%% Normalizing the observed result

M1M2Exp = M1.*M2;
M1M2Abs = abs(M1M2);
A = M1M2Abs(1,1) / M1M2Exp(1,1);
output = M1M2Abs ./ A;

subplot(1,2,1) 

imagesc(M1.*M2); axis square; title('Expected Result');
colorbar;

subplot(1,2,2) 
imagesc(output); axis square; title('Observed Result');
colorbar;

%% Compare the difference
% %{
M1M2Exp = M1.*M2;

normM1M2Exp = M1M2Exp - min(M1M2Exp(:));
normM1M2Exp = normM1M2Exp ./ max(normM1M2Exp(:))

figure;
subplot(1,2,1) 

imagesc(normM1M2Exp); axis square; title('Expected Result');
colorbar;

M1M2 = F(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
     Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2);
M1M2 = abs(M1M2);

normM1M2 = M1M2 - min(M1M2(:));
normM1M2 = normM1M2 ./ max(normM1M2(:))
 
subplot(1,2,2) 
imagesc(normM1M2); axis square; title('Observed Result');
colorbar;


absError = abs(normM1M2 - normM1M2Exp)
relError = (absError ./ abs(normM1M2Exp))


%% Plot Reults
figure;
subplot(1,3,1)
%Plot SLM 1
figure; 
imagesc(SLM1);
axis square;
title('SLM1');
xticks(''); yticks('');
colormap gray;

%Plot SLM2
figure;
subplot(1,3,2)
imagesc(SLM2);
axis square;
title('SLM2');
xticks(''); yticks('');
colormap gray;

%Plot camera
subplot(1,4,3) 
imagesc(M1.*M2); 
axis square; 
title('Expected Result');

M1M2 = F(Nx/2 - size(M1,1)/2+1:Nx/2 + size(M1,1)/2, ...
     Nx/2 - size(M1,2)/2+1:Nx/2 + size(M1,2)/2);
 
subplot(1,3,3) 
imagesc(abs(M1M2)); 
axis square; 
title('Observed Result');

%}

%mse(output, M1M2Exp)
% D = abs(output - M1M2Exp).^2; MSE = sum(D(:))/numel(output)
