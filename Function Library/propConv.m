function [F, x] = propConv(F, x, lambda, f, SLM1, SLM2)
% propConv computes the convolution using Fourier-based simulation
% Author : YCW
% Based on Fresnel propagation
% TODO: Do a sanity check on the model

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Illumnation of SLM1

F = F.*SLM1; % Diffraction field of SLM1

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Illumination of SLM2

F = F.*SLM2; % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, f); % Incident on camera

end

