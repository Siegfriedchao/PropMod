function FPool = compMaxPool(FReLU, Nx, X, psize, pstride)
% Apply maxpooling function to the matrix in place
% Author: YCW
% Inputs: 
%

% Max pooling
FPool = zeros(round((size(FReLU, 1) - psize + 1) / pstride), ...
    round((size(FReLU, 2) - psize + 1) / pstride ));

r2 = 1;
for r = 1:pstride:(size(FReLU, 1) - psize)
   c2 = 1;
   for c = 1:pstride:(size(FReLU, 2) - psize)
       FPool(r2, c2) = max(FReLU(r: r+psize, c:c+psize), [], 'all');
       c2 = c2 + 1;
   end
   r2 = r2 + 1;
end

end

