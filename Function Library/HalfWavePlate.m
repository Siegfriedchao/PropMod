function RetVal = HalfWavePlate(theta)

    % Theta = rotation angle of fast axis with respect to x axis

    RetVal = [cos(theta)^2 - sin(theta)^2, 2*cos(theta)*sin(theta) ; ...
        2*cos(theta)*sin(theta), sin(theta)^2 - cos(theta)^2];
    
    RetVal = RetVal * exp(-1i*pi/2);

end

