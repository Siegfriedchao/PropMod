function SLM = LoadSLMDisplay(Nx, X, M2, SLMNo)
%LoadSLMDisplay loads the image for display on SLMs
%   Nx: User input number of pixels
%   X: Image

if SLMNo == 1
    SLM = zeros(Nx);
    SLM(Nx/2 - size(X,1)/2+1:Nx/2 + size(X,1)/2, ...
         Nx/2 - size(X,2)/2+1:Nx/2 + size(X,2)/2) = X;  % Applying X to SLM1
elseif SLMNo == 2
    SLM = zeros(Nx);

    M2Pad = zeros(size(X,1), size(X,2));
    M2Pad(size(X,1)/2 - size(M2,1)/2 : size(X,1)/2 + size(M2,1)/2 - 1, ...
            size(X,2)/2 - size(M2,2)/2 : size(X,2)/2 + size(M2,2)/2 - 1) = M2;
    M2Pad = abs(fft2(M2Pad));

    SLM(Nx/2 - round(size(M2Pad,1)/2)  : Nx/2 + round(size(M2Pad,1)/2) - 1, ...
        Nx/2 - round(size(M2Pad,1)/2)  : Nx/2 + round(size(M2Pad,1)/2) - 1) = M2Pad;

end

end

