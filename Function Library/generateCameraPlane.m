function camera = generateCameraPlane(pitch_cam, size_cam, CAM_x, CAM_y, sim_res)
    
    camera.pixel_gap = pitch_cam - size_cam;
    source_x_cam = CAM_x * size_cam + (CAM_x - 1) * camera.pixel_gap;
    camera.x_res =  source_x_cam / sim_res;
    source_y_cam = CAM_y * size_cam + (CAM_y - 1) * camera.pixel_gap;
    camera.y_res = source_y_cam / sim_res;
    
    camera.x_res = uint16(camera.x_res);
    camera.y_res = uint16(camera.y_res);
    camera.plane = zeros(camera.x_res, camera.y_res);
    
end