function FReLU = compReLU(F)
%compReLU apply ReLU
%

FReLU = real(F);
FReLU(FReLU < 0) = 0;

end

