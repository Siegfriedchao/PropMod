function Output = compHadamardMultiWithNeg(MM1, MM2, Nkn)
% Compute Hadamard product (matrix-matrix element wise multiplication)
% Author: YCW
% Inputs: M1, M2 matrices of the same dimention, currently arranged by a Nkn 
% by Nkn pattern. Values could be negative.


%% User-entered parameters

lambda = 633e-9;
f = 1;

Nk = size(MM1, 1); % size of kernel
Ng = 10; % size of gap
Nx = Ng + (Nk + Ng) * Nkn;

Nn = Nx * 3;

% Pre-processing
M1p = zeros(Nk, Nk, Nkn, Nkn);
M1n = zeros(Nk, Nk, Nkn, Nkn);

M2p = zeros(Nk, Nk, Nkn, Nkn);
M2n = zeros(Nk, Nk, Nkn, Nkn);

% Set SLM1
% filling SLM1 with multiple kernels
for r = 1:Nkn
    for c = 1:Nkn
       M1p(:,:,r,c) = MM1(:,:,r,c) + abs(min(MM1(:,:,r,c),[],'all')) + 0.00001;
       M1n(:,:,r,c) = ones(Nk, Nk) * abs(min(MM1(:,:,r,c),[],'all')) + 0.00001;
    end
end 

M1 = [M1p, M1n; M1p, M1n];

S1 = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       S1(Ng + 1 + (Nk * 2 + Ng)*(r-1): Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2) = M1(:,:, r, c);
    end
end

SLM1 = zeros(Nn);
SLM1(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2) = S1;
 
% Set SLM2
for r = 1:Nkn
    for c = 1:Nkn
       M2p(:,:,r,c) = MM2(:,:,r,c) + abs(min(MM2(:,:,r,c),[],'all')) + 0.00001;
       M2n(:,:,r,c) = ones(Nk, Nk) * abs(min(MM2(:,:,r,c),[],'all')) + 0.00001;
    end
end 

M2 = [M2p, M2p; M2n, M2n];

S2 = zeros(Nx);

for r = 1:Nkn
    for c = 1:Nkn
       S2(Ng + 1 + (Nk * 2 + Ng)*(r-1): Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2) = M2(:,:, r, c);
    end
end

SLM2 = zeros(Nn);
SLM2(Nn/2 - size(S2,1)/2:Nn/2 + size(S2,1)/2-1, ...
     Nn/2 - size(S2,2)/2:Nn/2 + size(S2,2)/2-1) = S2;

%% Calculations

xx = 5 * Nn;
x = linspace(-4000e-5, 4000e-5, xx);
SMF = CreateSMF(x);
F = SMF.F; % This is the beam out of the SMF

[F, x] = propFresnel(F, x, lambda, f);
F = propLens(F, x, lambda, f);

F = F(xx/2 - Nn/2: xx/2 + Nn/2 - 1, ...
        xx/2 - Nn/2: xx/2 + Nn/2 - 1);

F = F.*SLM1; % Diffraction field of SLM1

x = linspace(-1000e-5, 1000e-5, Nn);

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,x] = propFresnel(F, x, lambda, 2*f); % Illumination of SLM2

F = F.*flipud(fliplr(SLM2)); % Image is flipped, so flip matrix by which we're multiplying.
                             % Diffraction field of SLM2

[F, x] = propFresnel(F, x, lambda, 2*f);
F = propLens(F, x, lambda, f);
[F,~] = propFresnel(F, x, lambda, 2*f); % Incident on camera

M1M2 = abs(F);

%% Normalizing the observed result
% TODO: Currently we are only taking a single value for normalization, a 
% better method should be in place

% Compute expected result
Expect = zeros(Ng + (Nk * 2 + Ng) * Nkn);

for r = 1:Nkn
    for c = 1:Nkn
       Expect(Ng + 1 + (Nk * 2 + Ng)*(r-1): ...
           Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): ...
          Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2) ...
          = M1(:,:, r, c).* M2(:,:, r, c);
    end
end

% Post processing

cropRegion = M1M2(Nn/2 - size(S1,1)/2+1:Nn/2 + size(S1,1)/2, ...
     Nn/2 - size(S1,2)/2+1:Nn/2 + size(S1,2)/2);
 
cropResult = zeros(Nk*2, Nk*2, Nkn, Nkn);
for r = 1:Nkn
    for c = 1:Nkn
        cropResult(:,:, r, c) = ...
          cropRegion(Ng + 1 + (Nk * 2 + Ng)*(r-1):...
           Ng + (Nk * 2 + Ng)*(r-1) + Nk * 2, ...
          Ng + 1 + (Nk * 2 + Ng)*(c-1): ...
           Ng + (Nk * 2 + Ng)*(c-1) + Nk * 2);
    end
end

finalResult = zeros(Nk, Nk, Nkn, Nkn);
for r = 1:Nk
    for c = 1:Nk
        finalResult(r, c, :, :) = cropResult(r, c, :, :)...
                    - cropResult(r, c + Nk, :, :)...
                    - cropResult(r+Nk, c, :, :) ...
                    + cropResult(r+Nk, c+Nk, :, :);
    end    
end


%% Normalizing the observed result
Output = zeros(Nk, Nk, Nkn, Nkn);
M1M2Exp = MM1 .* MM2;
M1M2Abs = finalResult;
for rk = 1:Nkn
    for ck = 1:Nkn
        M1M2ExpRC = M1M2Exp(:,:,rk,ck);
        [~, I] = max(M1M2ExpRC(:));
        [Ir, Ic] = ind2sub(size(M1M2ExpRC),I);
        A = M1M2Abs(Ir,Ic,rk,ck) / M1M2ExpRC(Ir,Ic);
        Output(:,:,rk,ck) = M1M2Abs(:,:,rk,ck) ./ A; 
    end
end

end