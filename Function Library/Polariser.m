function RetVal = Polariser(theta)

    % Theta is angle from x axis

    RetVal = [cos(theta)^2, cos(theta)*sin(theta); ...
        cos(theta)*sin(theta), sin(theta)^2];

end