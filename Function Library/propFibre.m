function F_out = propFibre(F, x, Fibre, ModeNo)
    % PROPFIBRE Propagation through fiber
    % Author: RPM
    % Inputs:
    %   F : Fields incident on fiber facet
    %   x : coordinates of fields incident on fiber facet
    %   Fibre : structure with fiber parameters
    %   ModeNo : mode number for which coupling coefficients are reported.
    % Outputs:
    %   F_out : fields out of the distal fiber end

    F_out = zeros(size(F)); % Preallocate
    
    % Build mask to ignore all fields outside of core.
    [x_mesh, y_mesh] = meshgrid(x, x);
    r2_mesh = x_mesh.^2 + y_mesh.^2;
    
    c = zeros(1, length(Fibre.L));

    for idx = 1:length(Fibre.L)
        
        % Interpolate fiber mode down to resolution of incident fields
        FibreMode = squeeze(Fibre.F(idx,:,:));
        FibreMode = interp2(Fibre.x, Fibre.x', FibreMode, x, x');
        FibreMode(r2_mesh > Fibre.CoreDiameter^2/4) = 0;
        
        % Calculate coupling coefficient
        c(idx) = abs(sum(sum(F.*conj(FibreMode)))).^2;
        c(idx) = c(idx)/sum(sum(abs(F).^2))/sum(sum(abs(FibreMode).^2));
        powerc = sum(sum(abs(F).^2))/sum(sum(abs(FibreMode).^2));
        
        % Add contribution to outgoing fields
        F_out = F_out + sqrt(c(idx)*powerc) * FibreMode * ...
            exp(1i*Fibre.beta(idx)*Fibre.Length);
        
    end
    
    disp(['MMF Mode ' num2str(ModeNo)])
    disp(['  Modal Coupling Coefficient: ' num2str(c(ModeNo))])
    disp(['  Relative Modal Coupling Coefficient: ' num2str(c(ModeNo)/sum(c))])

end