function SLM = generateSLMPlane(pixel_pitch, pixel_size, SLM_x, SLM_y, simulation_resolution)
    pixel_gap = pixel_pitch - pixel_size;
    source_x = SLM_x * pixel_size + (SLM_x - 1) * pixel_gap;
    source_x_res = source_x / simulation_resolution;
    source_y = SLM_y * pixel_size + (SLM_y - 1) * pixel_gap;
    source_y_res = source_y / simulation_resolution;

    % store E_x and E_y, would need uint32 to avoid overflow
    SLM.source_plane = zeros(uint32(source_x_res), uint32(source_y_res), 2); 
    
    % Do not compute the gaps, list indices to be processed
    SLM.compute_x = [];
    T1 = uint16(pixel_size/simulation_resolution);
    T2 = uint16(pixel_gap/simulation_resolution);
    for x_x = 1:SLM_x
        SLM.compute_x(end+1:end+T1) = (x_x-1)*T1 + (x_x-1)*T2 + 1 ...
             : x_x*T1 + (x_x-1)*T2;
    end
    
    SLM.compute_y = [];
    for y_y = 1:SLM_y
        SLM.compute_y(end+1:end+T1) = (y_y-1)*T1 + (y_y-1)*T2 + 1 ...
             : y_y*T1 + (y_y-1)*T2;
    end

    SLM.compute_x = uint16(SLM.compute_x);
    SLM.compute_y = uint16(SLM.compute_y);

    SLM.compute_x_zero = 1 : source_x_res;
    SLM.compute_x_zero(SLM.compute_x) = []; %remove indices

    SLM.compute_y_zero = 1 : source_y_res;
    SLM.compute_y_zero(SLM.compute_y) = []; %remove indices
    
    SLM.compute_x_zero = uint16(SLM.compute_x_zero);
    SLM.compute_y_zero = uint16(SLM.compute_y_zero);

    SLM.source_x_res = uint16(source_x_res);
    SLM.source_y_res = uint16(source_y_res);
end