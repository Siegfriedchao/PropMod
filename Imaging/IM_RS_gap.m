% Youchao Wang
% University of Cambridge
% January 2023

clc; clear all; close all;
addpath('../Function Library');

%% User-entered parameters

lambda = 633e-9;

% Set SLM1

pixel_pitch = 8e-6;
pixel_size = 7.8e-6;
simulation_resolution = 1e-7;
SLM_x = 80;
SLM_y = SLM_x;

pixel_gap = pixel_pitch - pixel_size;
source_x = SLM_x * pixel_size + (SLM_x - 1) * pixel_gap;
source_x_res = (source_x / simulation_resolution);
source_y = SLM_y * pixel_size + (SLM_y - 1) * pixel_gap;
source_y_res = uint32(source_y / simulation_resolution);

% load('mandrill', 'X');
X = imread('/Users/youchaowang/Documents/GitLab/python_scripts/CGH/input/checkerboard_512.bmp');


T1 = uint32(pixel_size/simulation_resolution);
T2 = uint32(pixel_gap/simulation_resolution);
X = imresize(X, 1/16, 'nearest');
IM = imresize(X, T1, 'nearest');

IM_with_gap = zeros(size(IM,1)+(size(X,1)-1)*T2, size(IM,2)+(size(X,2)-1)*T2);

place_x = [];
place_y = [];
for x_x = 1:size(X,1)
    place_x(end+1:end+T1) = uint32((x_x-1)*T1 + (x_x-1)*T2 + 1) ...
         : uint32(x_x*T1 + (x_x-1)*T2);
end

for y_y = 1:size(X,2)
    place_y(end+1:end+T1) = uint32((y_y-1)*T1 + (y_y-1)*T2 + 1) ...
         : uint32(y_y*T1 + (y_y-1)*T2);
end

for x_x = 1:length(place_x)
    for y_y = 1:length(place_y)
        IM_with_gap(place_x(x_x), place_y(y_y)) = IM(x_x, y_y);
    end
end

A = IM_with_gap;
[M, N] = size(A);
A = flipud(A);
Ig = single(A);
Ig = Ig/max(Ig, [], "all");

ug = sqrt(Ig);
L = 0.3e-3;
du = L/M;
u = -L/2:du:L/2 -du;
v = u;


%% Define imaging system parameter
% lambda = 0.5e-6;
wxp = 25e-3; %exit pupil radius
zxp = 125e-3; %exit pupil distance
f0 = wxp / (lambda*zxp);
% f0 = 1;

fu = -1/(2*du):1/L:1/(2*du)-(1/L);
fv = fu;
[Fu, Fv] = meshgrid(fu,fv);
H = circ(sqrt(Fu.^2 + Fv.^2) / f0);

surf(H.*0.99)
lighting phong
shading interp

%% Generate image
H = fftshift(H);
% surf(H.*0.99)
% lighting phong
% shading interp
Gg = fft2(fftshift(ug));
Gi = Gg.*H;
ui = ifftshift(ifft2(Gi));

Ii = abs(ui).^2;
figure
imagesc(flipud(nthroot(Ii,2)))
colormap gray

function RetVal = circ(r)
    RetVal = abs(r) <= 1;
end