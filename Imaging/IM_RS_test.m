% Youchao Wang
% University of Cambridge
% January 2023

clc; clear all; close all;
addpath('../Function Library');

A = imread('/Users/youchaowang/Documents/GitLab/python_scripts/CGH/input/USAF.bmp');
A = imcrop(A,[60 40 499 499]);
[M, N] = size(A);
A = flipud(A);
Ig = single(A);
Ig = Ig/max(Ig, [], "all");

% ug = sqrt(Ig);
% adding phase noise (speckle)
ug = sqrt(Ig).*exp(1i * 2 * pi * rand(M));
L = 0.3e-3;
du = L/M;
u = -L/2:du:L/2 -du;
v = u;

% imagesc(u, v, Ig)

%% Define imaging system parameter
lambda = 0.638e-6;
% Assuming focal length 100mm, diameter 25mm
wxp = 25e-3; %exit pupil radius
zxp = 100e-3; %exit pupil distance
f0 = wxp / (lambda*zxp);

fu = -1/(2*du):1/L:1/(2*du)-(1/L);
fv = fu;
[Fu, Fv] = meshgrid(fu,fv);
H = circ(sqrt(Fu.^2 + Fv.^2) / f0);

% surf(H.*0.99)
% lighting phong
% shading interp

%% Generate image
H = fftshift(H);
% surf(H.*0.99)
% lighting phong
% shading interp
Gg = fft2(fftshift(ug));
Gi = Gg.*H;
ui = ifftshift(ifft2(Gi));

Ii = abs(ui).^2;
figure
imagesc(flipud(nthroot(Ii,2)))
colormap gray
axis square
xlabel('Pixel')
ylabel('Pixel')
set(gca,'FontSize',22, 'FontName', 'Arial')

figure
imagesc(flipud(nthroot(Ii(90:260,265:435),2)))
colormap gray
axis square
xlabel('Pixel')
ylabel('Pixel')
set(gca,'FontSize',22, 'FontName', 'Arial')


function RetVal = circ(r)
    RetVal = abs(r) <= 1;
end