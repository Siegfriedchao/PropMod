% Youchao Wang
% University of Cambridge
% January 2023

clc; clear all; close all;
addpath('/Users/youchaowang/Documents/GitLab/propagation-model/Function Library');
addpath('/Users/youchaowang/Documents/GitLab/propagation-model/Jones Matrix/Function Library');

%% Pre-configuration
rng(1107, 'twister')

%% User-defined parameters
theta = 89 * pi / 180;
theta2 = 48 * pi / 180;
pol1 = 26 * pi / 180;
pol2 = 40 * pi / 180;
hwp = 30 * pi / 180;

simulation_resolution = 1e-8;

% Currently we do not consider the camera
% CAM_x = 4;
% CAM_y = 4;
% pixel_pitch_cam = 5e-6;
% pixel_size_cam = 4.8e-6;
% camera_depth = 256;

% Small pixel counts
SLM_x = 4;
SLM_y = 4;
pixel_pitch = 8e-6;
pixel_size = 7.5e-6;

% SLM_x2 = 16;
% SLM_y2 = 16;
% pixel_pitch2 = 7.8e-6;
% pixel_size2 = 7.8e-6;

USE_GAUSSIAN = true;
range = 200e-6;
w0 = 8.2e-4;

% Simulating Santec, where phase stability is ~0.003pi. Currently 0.3pi
% etaAll = 2.* pi .* (8+ 3.2.*round(2.*rand(SLM_x, SLM_y)-1))./32;
etaAll = 2.* pi .* (8+ 13.2.*round(2.*rand(SLM_x, SLM_y)-1))./32;
% etaAll2 = 2.* pi .* (9+ 3.2.*round(2.*rand(SLM_x2, SLM_y2)-1))./32;
% etaAll = 2.* pi .* 8./32 + pi* 0.003.* (2.*rand(SLM_x, SLM_y)-1);
% etaAll2 = 2.* pi .* 8./32 + pi* 0.003.* (2.*rand(SLM_x2, SLM_y2)-1);

%% Create the source plane

SLM1 = generateSLMPlane(pixel_pitch, pixel_size, SLM_x, SLM_y, simulation_resolution);

source_plane = SLM1.source_plane;
Modulation = zeros(uint16(SLM1.source_x_res), uint16(SLM1.source_y_res));
iCnt = uint16(length(SLM1.compute_y) / SLM_y);
jCnt = uint16(length(SLM1.compute_x) / SLM_x);
i = 0;
j = 0;
xx = 1;
yy = 1;

%% Model Gaussian beam
%https://physics.stackexchange.com/questions/68494/polarization-of-the-lowest-mode-of-a-gaussian-beam
%https://physics.stackexchange.com/questions/126711/proper-and-rigourous-derivation-of-gaussian-beam
x = linspace(-range, range, size(source_plane, 1));
SMF = generateSMF(x, w0);

for SLMx = SLM1.compute_x
    for SLMy = SLM1.compute_y
        % Assume vertially polarised beam, [0;1]
        % eta should be changed only when we change a pixel

        eta = etaAll(xx, yy);

        i = i + 1;
        if i == iCnt
            i = 0;
            yy = yy + 1;
        end

        E_in = [(1-exp(1i* eta))*sin(theta)*cos(theta);...
            sin(theta)*sin(theta)+cos(theta)*cos(theta)*exp(1i*eta)];
        
        if USE_GAUSSIAN
            E_in = E_in * SMF.F(SLMx, SLMy);
        end
        
        E_out = Polariser(pol1) * E_in;
        E_out = HalfWavePlate(hwp) * E_out;
        
        source_plane(SLMx, SLMy, 1) = E_out(1);
        source_plane(SLMx, SLMy, 2) = E_out(2);
        % Total intensity
        Modulation(SLMx, SLMy) = ...
            E_out(1).*conj(E_out(1)) + E_out(2).*conj(E_out(2));
    end
    
    yy = 1;
    j = j + 1;
    if j == jCnt
        j = 0;
        xx = xx + 1;
    end


end

%%
% add phase noise here
phase_noise = exp(1i * 0.2 * pi * rand(SLMx));

ug_x = source_plane(:,:,1) .* phase_noise;
ug_y = source_plane(:,:,2) .* phase_noise;

M = SLMx;
L = double(SLMx) * simulation_resolution;

du = simulation_resolution;
u = -L/2:du:L/2 -du;
v = u;

% imagesc(u, v, Ig)

%% Define imaging system parameter
lambda = 0.638e-6;
% Assuming focal length 100mm, diameter 25mm
wxp = 25e-3; %exit pupil radius
zxp = 100e-3; %exit pupil distance
f0 = wxp / (lambda*zxp);

fu = -1/(2*du):1/L:1/(2*du)-(1/L);
fv = fu;
[Fu, Fv] = meshgrid(fu,fv);
H = circ(sqrt(Fu.^2 + Fv.^2) / f0);

% surf(H.*0.99)
% lighting phong
% shading interp

%% Generate image
H = fftshift(H);
% surf(H.*0.99)
% lighting phong
% shading interp
Gg = fft2(fftshift(ug_x));
Gi = Gg.*H;
ui = ifftshift(ifft2(Gi));

Gg_y = fft2(fftshift(ug_y));
Gi_y = Gg_y.*H;
ui_y = ifftshift(ifft2(Gi_y));

Ii = ui.*conj(ui) + ui_y.*conj(ui_y);
% Ii = abs(ui).^2;

%%
figure
imagesc(u*10^6, v*10^6, Ii)
axis square
axis xy
xlabel('Horizontal (\mum)')
ylabel('Vertical (\mum)')
colorbar
set(gca,'FontSize',22, 'FontName', 'Arial')

%%
figure
vvalue = -0.4e-5;
vindex = round(vvalue/du+(M/2 + 1));

% find the average
avgLine = nan(1, SLMx);
for pix = 1:SLM_x
    avgLine((pix-1)*(750+50)+1:(pix-1)*(750+50)+750) = mean(Ii(vindex, (pix-1)*(750+50)+1:(pix-1)*(750+50)+750));
end

plot(u*10^6,Ii(vindex, :), u*10^6, Modulation(vindex,:),':', u*10^6, avgLine);
xlabel('Horizontal (\mum)')
ylabel('Intensity')
legend('Simulated','Ideal','Averaged','Location','Best','FontSize',18)
set(gca,'FontSize',22, 'FontName', 'Arial')

% plot mesh
% Z = Ii;
% Z(Z<=0.1) = nan;
% mesh(Z)

function RetVal = circ(r)
    RetVal = abs(r) <= 1;
end

function SMF = generateSMF(x, w0)
    SMF.x = x;
    SMF.w0 = w0; % beam waist
    
    % Gaussian beam from SMF
    y = x';
    SMF.F = exp(2*(-x.^2 - y.^2)/SMF.w0^2);
    SMF.F = SMF.F/max(SMF.F, [], 'all');  
end