% Ralf Mouthaan, Youchao Wang
% University of Cambridge
% February 2022
%
% Script to investigate response of SLMs using Jones matrices
% Acknowledging help from Andrew Kadis
%
% Ref. ﻿Study of a nematic zero-twist liquid crystal spatial light modulator
% Ref. ﻿Liquid crystal devices in adaptive optics. Phillip Michael Birch

clc; clear variables; close all;

% 

%% BlueJay as a phase-modulating device

% Assuming vertically polarised input polariser
% Assuming horizontally polarised output polariser

E_in = [0; 1]; % Vertically polarised

for Voltage = 0:1
    
    JonesMatrixFLC = BlueJay(Voltage);
    E_out = JonesMatrixFLC*E_in;
    Modulation(Voltage+1) = E_out(1); % Extract horizontal
    
end
% 
% figure;
% plot(real(Modulation), imag(Modulation), 'rx');
% xlim([-1 1]); ylim([-1 1]);
% axis square;
% xlabel('Real'); ylabel('Imaginary');
% title('BlueJay as a phase modulator');

%% Bluejay as an amplitude modulating device 1

% Input polariser vertical
% Output polariser at 45 degrees

E_in = [0; 1]; % Vertically polarised
% E_in = HalfWavePlate(pi/8) * E_in;

for Voltage = 0:1
    
    JonesMatrixFLC = BlueJay(Voltage);
    E_out = JonesMatrixFLC*E_in;
    E_out = Polariser(pi/4) * E_out; % Extract 45 degree
    Modulation(Voltage + 1) = norm(E_out);
    
end

% figure;
% plot(real(Modulation), imag(Modulation), 'rx');
% xlim([-1 1]); ylim([-1 1]);
% axis square;
% xlabel('Real'); ylabel('Imaginary');
% title('BlueJay as an amplitude modulator');

%% Bluejay as an amplitude modulating device 2

% Input polariser vertical
% Output polariser vertical
E_in = [0; 1];


for Voltage = 0:1
    
    JonesMatrixFLC = BlueJay(Voltage);
    E_out = JonesMatrixFLC * E_in;
    E_out = HalfWavePlate(pi/8) * E_out;
    Modulation(Voltage + 1) = E_out(2); % extract vertical
    
end

% figure;
% plot(real(Modulation), imag(Modulation), 'rx');
% xlim([-1 1]); ylim([-1 1]);
% axis square;
% xlabel('Real'); ylabel('Imaginary');
% title('BlueJay as an amplitude modulator');


%% Aurora as a phase-modulating device

% Assuming vertically polarised input polariser
% Assuming horizontally polarised output polariser

E_in = [0; 1]; % Vertically polarised

for Voltage = 0:255
    
    JonesMatrixNLC = Aurora(Voltage);
    E_out = JonesMatrixNLC * E_in;
    Modulation(Voltage+1) = E_out(1); % Extract horizontal
    
end

% figure;
% plot(real(Modulation), imag(Modulation), 'rx');
% % xlim([-1 1]); ylim([-1 1]);
% axis square;
% xlabel('Real'); ylabel('Imaginary');
% title('Aurora as a phase modulator');


%% Aurora as an amplitude modulating device

E_in = [1; 1]; % Vertically polarised
E_in = Polariser(pi/4) * E_in;

for Voltage = 0:255
    
    JonesMatrixNLC = Aurora(Voltage);
    E_out = JonesMatrixNLC*E_in;
    E_out = Polariser(pi/4) * E_out; % Extract 45 degree
    Modulation(Voltage + 1) = norm(E_out);
    
end

% figure;
% plot(real(Modulation), imag(Modulation), 'rx');
% xlim([-1 1]); ylim([-1 1]);
% axis square;
% xlabel('Real'); ylabel('Imaginary');
% title('Aurora as an amplitude modulator');

% figure;
% plot(0:255, abs(Modulation).^2, 'rx');
% xlim([0 255]);
% axis square;
% xlabel('bit value'); ylabel('intensity');
% title('Aurora as an amplitude modulator');

%% Properly characterising a NLC device
% The response for a nematic liquid crystal can be theoretically calculated
% using expressions for the molecular rotation.

E_in = [1; 1]; % Vertically polarised
alpha = pi / 4;
for Voltage = 0:255:5
    
    JonesMatrixNLC = AuroraComplete(Voltage);
    P = [0,0;0,1];
    E_out = P * Pol(-alpha) * JonesMatrixNLC * Pol(alpha) * E_in
    Modulation(Voltage + 1) = norm(E_out);
    
end

figure;
plot(0:255, abs(Modulation).^2, 'rx');
xlim([0 255]);
axis square;
xlabel('bit value'); ylabel('intensity');
title('Aurora as an amplitude modulator');

function RetVal = Pol(alpha)
    RetVal = [cos(alpha), sin(alpha); -sin(alpha), cos(alpha)];
end

function RetVal = EinPol(alpha)
    RetVal = [cos(alpha); sin(alpha)];
end

function RetVal = AuroraComplete(Voltage)
    
    beta = pi * 1 / 660 * (neff(Voltage, 1.7, 1.5) - 1.5);% n_0=1.5
    RetVal = [1, 0; 0, exp(-1i * beta)];
end

function n_eff = neff(Voltage, n_e, n_0) %n_e=1.7, n_0=1.5
    theta = pi / 2 - 2 * atan(exp(-Voltage));
    n_eff = sqrt(1 / ((cos(theta)^2 / n_e^2) + (sin(theta)^2 / n_0^2)));

end

%%

function RetVal = ArbitraryRetardation(eta, theta, phi)

    % Equation taken from Wikipedia article - Jones Calculus
    % eta is the relative phase retardation induced between the fast axis
    % and slow axis
    % theta is the orientation of the fast axis with respect to the x-axis
    % phi is the circularity
    
    RetVal = [cos(theta)^2 + exp(1i*eta)*sin(theta)^2, ...
              (1-exp(1i*eta)) * exp(-1i*phi)*cos(theta)*sin(theta) ; ...
              (1-exp(1i*eta)) * exp( 1i*phi)*cos(theta)*sin(theta) , ...
              sin(theta)^2 + exp(1i*eta)*cos(theta)^2];
          
    RetVal = exp(-1i*eta/2) * RetVal;
    
end

function RetVal = HalfWavePlate(theta)

    % Theta = rotation angle of fast axis with respect to x axis

    RetVal = [cos(theta)^2 - sin(theta)^2, 2*cos(theta)*sin(theta) ; ...
        2*cos(theta)*sin(theta), sin(theta)^2 - cos(theta)^2];
    
    RetVal = RetVal * exp(-1i*pi/2);

end

function RetVal = QuarterWavePlate(theta)

    % Theta = rotation angle of fast axis with respect to x axis

    RetVal = [cos(theta)^2 + 1i*sin(theta)^2, ...
              (1-1i)*sin(theta)*cos(theta) ; ...
              (1-1i)*sin(theta)*cos(theta) , ...
              sin(theta)^2 + 1i*cos(theta)^2 ];
          
    RetVal = RetVal*exp(-1i*pi/4);

end

function RetVal = BlueJay(Voltage)
    
    % Returns Jones matrix for Bluejay
    
    if Voltage == 0
        RetVal = HalfWavePlate(pi/2 - pi/8);
    elseif Voltage == 1
        RetVal = HalfWavePlate(pi/2 + pi/8);
    else
        error('BlueJay is a binary device that only accepts voltages of 0 or 1');
    end

end

function RetVal = Aurora(Voltage)

    % Returns Jones matrix for Multi-phase device
    % Assuming a phase modulation of 1.6pi can be achieved
    if floor(Voltage) ~= Voltage && Voltage > 255 || Voltage < 0
        error('xxx');
    else
%         RetVal = ArbitraryRetardation(-0.4*pi + 1.6* pi * Voltage/255, 0 ,0);
        RetVal = ArbitraryRetardation(2* pi * Voltage/255, 0 ,0);
    end

end

function RetVal = Polariser(theta)

    % Theta is angle from x axis

    RetVal = [cos(theta)^2, cos(theta)*sin(theta); cos(theta)*sin(theta), sin(theta)^2];

end


