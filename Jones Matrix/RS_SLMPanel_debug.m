% Youchao Wang
% University of Cambridge
% 2022-2023

clc; clear variables; close all;
addpath('/Users/youchaowang/Documents/GitLab/propagation-model/Jones Matrix/Function Library');

%% Pre-configuration
rng(1107, 'twister')

%% User-defined parameters
theta = 89 * pi / 180;
theta2 = 48 * pi / 180;
pol1 = 26 * pi / 180;
pol2 = 40 * pi / 180;
hwp = 30 * pi / 180;

simulation_resolution = 1e-7;

% CAM_x = 80;
% CAM_y = 80;
% pixel_pitch_cam = 5e-6;
% pixel_size_cam = 4.8e-6;
% camera_depth = 256;
% 
% SLM_x = 64;
% SLM_y = 64;
% pixel_pitch = 8e-6;
% pixel_size = 7.8e-6;
% 
% SLM_x2 = 64;
% SLM_y2 = 64;
% pixel_pitch2 = 7.8e-6;
% pixel_size2 = 7.6e-6;
CAM_x = 28;
CAM_y = 28;
pixel_pitch_cam = 5e-6;
pixel_size_cam = 4.8e-6;
camera_depth = 256;

SLM_x = 16;
SLM_y = 16;
pixel_pitch = 8e-6;
pixel_size = 7.8e-6;

SLM_x2 = 16;
SLM_y2 = 16;
pixel_pitch2 = 7.8e-6;
pixel_size2 = 7.6e-6;


USE_GAUSSIAN = true;

w0 = 8.2e-4;

% Simulating Santec, where phase stability is ~0.003pi. Currently 0.3pi
% etaAll = 2.* pi .* (8+ 3.2.*round(2.*rand(SLM_x, SLM_y)-1))./32;
% etaAll2 = 2.* pi .* (9+ 3.2.*round(2.*rand(SLM_x2, SLM_y2)-1))./32;
% etaAll = 2.* pi .* 8./32 + pi* 0.003.* (2.*rand(SLM_x, SLM_y)-1);
% etaAll2 = 2.* pi .* 8./32 + pi* 0.003.* (2.*rand(SLM_x2, SLM_y2)-1);
etaAll = 2.* pi .* (8+ 0.48*2.*rand(SLM_x, SLM_y)-1)./32;
etaAll2 = 2.* pi .* (9+ 0.48.*2.*rand(SLM_x2, SLM_y2)-1)./32;

%% Create the source plane

SLM1 = generateSLMPlane(pixel_pitch, pixel_size, SLM_x, SLM_y, simulation_resolution);

source_plane = SLM1.source_plane;
Modulation = zeros(uint16(SLM1.source_x_res), uint16(SLM1.source_y_res));
iCnt = uint16(length(SLM1.compute_y) / SLM_y);
jCnt = uint16(length(SLM1.compute_x) / SLM_x);
i = 0;
j = 0;
xx = 1;
yy = 1;

%% Model Gaussian beam
%https://physics.stackexchange.com/questions/68494/polarization-of-the-lowest-mode-of-a-gaussian-beam
%https://physics.stackexchange.com/questions/126711/proper-and-rigourous-derivation-of-gaussian-beam
range = simulation_resolution * round(size(source_plane,1)/2);
x = linspace(-range, range, size(source_plane, 1));
SMF = generateSMF(x, w0);

for SLMx = SLM1.compute_x
    for SLMy = SLM1.compute_y
        % Assume vertially polarised beam, [0;1]
        % eta should be changed only when we change a pixel

        eta = etaAll(xx, yy);

        i = i + 1;
        if i == iCnt
            i = 0;
            yy = yy + 1;
        end

        E_in = [(1-exp(1i* eta))*sin(theta)*cos(theta);...
            sin(theta)*sin(theta)+cos(theta)*cos(theta)*exp(1i*eta)];
        
        if USE_GAUSSIAN
            E_in = E_in * SMF.F(SLMx, SLMy);
        end
        
        E_out = Polariser(pol1) * E_in;
        E_out = HalfWavePlate(hwp) * E_out;
        
        source_plane(SLMx, SLMy, 1) = E_out(1);
        source_plane(SLMx, SLMy, 2) = E_out(2);
        % Total intensity
        Modulation(SLMx, SLMy) = ...
            E_out(1).*conj(E_out(1)) + E_out(2).*conj(E_out(2));
    end
    
    yy = 1;
    j = j + 1;
    if j == jCnt
        j = 0;
        xx = xx + 1;
    end


end

%% Second SLM, assuming panels size of SLM2 is smaller than SLM1

SLM2 = generateSLMPlane(pixel_pitch2, pixel_size2, SLM_x2, SLM_y2, simulation_resolution);
% SLM2.source_plane(:,:,1) = putBinA(source_plane(:,:,1), SLM2.source_plane(:,:,1));
% SLM2.source_plane(:,:,2) = putBinA(source_plane(:,:,2), SLM2.source_plane(:,:,2));

% Modulation = zeros(source_x_res, source_y_res);

Mask = SLM2.source_plane(:,:,1);
iCnt = uint16(length(SLM2.compute_y) / SLM_y2);
jCnt = uint16(length(SLM2.compute_x) / SLM_x2);
i = 0;
j = 0;
xx = 1;
yy = 1;

for SLMx = SLM2.compute_x
    for SLMy = SLM2.compute_y

        eta2 = etaAll2(xx, yy);

        i = i + 1;
        if i == iCnt
            i = 0;
            yy = yy + 1;
        end
    
        % source_plane should be SLM1 not SLM2 here, this is not a typo
        E_in = [source_plane(SLMx,SLMy, 1);source_plane(SLMx,SLMy, 2)];

        E_out = NLCRetardation(eta2, theta2) * E_in;        
        E_out = Polariser(pol2) * E_out;
       
        % Total intensity
        Modulation(SLMx, SLMy) = ...
            E_out(1).*conj(E_out(1)) + E_out(2).*conj(E_out(2));

        % attach one to mask
        Mask(SLMx, SLMy) = 1;
    end

    yy = 1;
    j = j + 1;
    if j == jCnt
        j = 0;
        xx = xx + 1;
    end

end

for SLMx = 1: SLM2.source_x_res
    for SLMy = 1: SLM2.source_y_res     
        % Total intensity
        source_plane(SLMx, SLMy, 1) = Mask(SLMx, SLMy)* source_plane(SLMx, SLMy, 1);
        source_plane(SLMx, SLMy, 2) = Mask(SLMx, SLMy)* source_plane(SLMx, SLMy, 2);
        Modulation(SLMx, SLMy) = Mask(SLMx, SLMy) * Modulation(SLMx, SLMy);
    end
end

% Normalise and quantise
MaxMod = max(Modulation,[], 'all');
Modulation = round(Modulation * (camera_depth-1) / MaxMod);

%% Define the image plane
camera_prop = generateCameraPlane(pixel_pitch_cam, pixel_size_cam, CAM_x, CAM_y, simulation_resolution);
image_plane = camera_prop.plane;

out = putBinA(image_plane, Modulation);

% Every camera pixel should be averaged
% This seems to align with Fig 6.31 per original submission
T1 = uint16(pixel_size_cam/simulation_resolution);
T2 = uint16(camera_prop.pixel_gap/simulation_resolution);
pixel = zeros(CAM_x, CAM_y);
for pixel_x = 1:CAM_x
    for pixel_y = 1:CAM_y
        pixel(pixel_x, pixel_y) = mean(out((pixel_x-1)*T1 + (pixel_x-1)*T2 + 1 ...
             : pixel_x*T1 + (pixel_x-1)*T2, ...
             (pixel_y-1)*T1 + (pixel_y-1)*T2 + 1 ...
             : pixel_y*T1 + (pixel_y-1)*T2), ...
             'all');
    end
end

%%
figure;
b = bar3(pixel);
for k = 1:length(b)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = 'interp';
end
zlim([0 255])
colorbar;axis square
xlabel('Horizontal pixel')
ylabel('Vertical pixel')
zlabel('Camera grey scale value')
set(gca,'FontSize',22, 'FontName', 'Arial')

% %% Generate images
% figure;
% imagesc(out)
% axis square
% xlabel('x');
% ylabel('y');
% 
% figure;
% Z = Modulation;
% Z(Z<=0) = nan;
% mesh(Z)

function SMF = generateSMF(x, w0)
    SMF.x = x;
    SMF.w0 = w0; % beam waist
    
    % Gaussian beam from SMF
    y = x';
    SMF.F = exp(2*(-x.^2 - y.^2)/SMF.w0^2);
    SMF.F = SMF.F/max(SMF.F, [], 'all');  
end
