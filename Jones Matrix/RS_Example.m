% Youchao Wang
% University of Cambridge
% 2022-2023

clc; clear variables; close all;
addpath('/Users/youchaowang/Documents/GitLab/propagation-model/Jones Matrix/Function Library');

theta = 89 * pi / 180;
theta2 = 48 * pi / 180;
pol1 = 26 * pi / 180;
pol2 = 40 * pi / 180;
hwp = 30 * pi / 180;

NN = 31;

for Voltage1 = 0:NN
    for Voltage2 = 0:NN
        eta = 2* pi * Voltage1/NN;
        eta2 = 2* pi * Voltage2/NN;
        E_in = [(1-exp(1i* eta))*sin(theta)*cos(theta);...
            sin(theta)*sin(theta)+cos(theta)*cos(theta)*exp(1i*eta)];
        E_out = Polariser(pol1) * E_in;
        E_out = HalfWavePlate(hwp) * E_out;
        E_out = NLCRetardation(eta2, theta2) * E_out;        
        E_out = Polariser(pol2) * E_out;
       
        % Total intensity
        Modulation(Voltage1+1, Voltage2+1) = ...
            E_out(1).*conj(E_out(1)) + E_out(2).*conj(E_out(2));
    end
end
MaxMod = max(Modulation,[], 'all');
Modulation = Modulation / MaxMod;

figure;
% set(gcf,'Visible','on')
surf(Modulation)
xlim([0, NN])
ylim([0, NN])
xlabel('SLM1 bit value');
ylabel('SLM2 bit value');
zlabel('Normalised intensity')
